﻿using System.Windows.Forms;

namespace CanetisRadar
{
	// Token: 0x02000003 RID: 3
	public partial class Overlay : global::System.Windows.Forms.Form
	{
		// Token: 0x0600000E RID: 14 RVA: 0x00002AFC File Offset: 0x00000CFC
		protected override void Dispose(bool disposing)
		{
			bool flag = disposing && this.components != null;
			if (flag)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		protected override CreateParams CreateParams
		{
			get
			{
				const int csNoclose = 0x200;

				var cp = base.CreateParams;
				cp.ClassStyle |= csNoclose;
				return cp;
			}
		}

		// Token: 0x0600000F RID: 15 RVA: 0x00002B34 File Offset: 0x00000D34
		private void InitializeComponent()
		{
            this.RadarBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.RadarBox)).BeginInit();
            this.SuspendLayout();
            // 
            // RadarBox
            // 
            //this.RadarBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;

            int screenWidth = 1880; // Screen width in pixels (adjust as needed for your screen resolution)
            int controlWidth = this.RadarBox.Width; // Width of the RadarBox control

            // Calculate the X-coordinate to position the control at the center horizontally
            int centerX = (screenWidth - controlWidth) / 2;

            // Set the Y-coordinate to 100 (as you've specified)
            int centerY = 100;

            this.RadarBox.Location = new System.Drawing.Point(centerX, centerY);
            this.RadarBox.Name = "RadarBox";
            this.RadarBox.Size = new System.Drawing.Size(150, 150);
            this.RadarBox.TabIndex = 0;
            this.RadarBox.TabStop = false;
            // 
            // Overlay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 512);
            this.Controls.Add(this.RadarBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //this.Location = new System.Drawing.Point(800, 150);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Overlay";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "SoundRadar Overlay";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Overlay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadarBox)).EndInit();
            this.ResumeLayout(false);

		}

		// Token: 0x04000011 RID: 17
		private global::System.ComponentModel.IContainer components = null;

		// Token: 0x04000012 RID: 18
		private global::System.Windows.Forms.PictureBox RadarBox;
	}
}
