﻿# CanetisRadar Advance Custom

A radar to detect which sound coming from in 7.1 surround sound,  to help people with SSD (Single Side Deafness) or People with hearing problem for multipurpose, forking [CanetisRadar-Improved](https://github.com/ensingerphilipp/CanetisRadar-Improved)

## Properties

* Framework		: NET Framework 4.0
* Programming Lang	: C# 3.0
* Tooling			: Visual Studio 16.0

## Setup

Here to the github project for further setup and origin project : [https://github.com/ensingerphilipp/CanetisRadar-Improved](https://github.com/ensingerphilipp/CanetisRadar-Improved)

## Developer Note

* This is a personal build specific for 1080p, 16:9, center-top position for radar
* Fast build only, later i will change it to more dynamic code
